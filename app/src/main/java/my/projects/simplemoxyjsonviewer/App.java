package my.projects.simplemoxyjsonviewer;

import android.app.Application;

import my.projects.simplemoxyjsonviewer.injection.AppComponent;
import my.projects.simplemoxyjsonviewer.injection.DaggerAppComponent;

public class App extends Application {

    private static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder().build();
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }

    public static void setAppComponent(AppComponent appComponent) {
        App.appComponent = appComponent;
    }
}
