package my.projects.simplemoxyjsonviewer.model.api.serialization;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import my.projects.simplemoxyjsonviewer.model.api.serialization.item.SampleItemDeserializer;
import my.projects.simplemoxyjsonviewer.model.api.serialization.item.SampleItemDeserializerFactory;
import my.projects.simplemoxyjsonviewer.model.data.Item;

//TODO remove redundant deserializations
public class SampleDataDeserializer implements JsonDeserializer<OrderedItemsWrapper> {

    private SampleItemDeserializerFactory deserializerFactory = new SampleItemDeserializerFactory();

    @Override
    public OrderedItemsWrapper deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        List<Item> items = new ArrayList<>();

        JsonObject root = json.getAsJsonObject();

        JsonArray view = root.getAsJsonArray("view");
        for (int i = 0; i < view.size(); i++) {
            String itemName = view.get(i).getAsString();
            JsonObject itemJson = findJsonItemWithName(root, itemName);
            SampleItemDeserializer<? extends Item> deserializer = deserializerFactory.getDeserializer(itemName);
            items.add(deserializer.deserialize(itemJson));
        }

        return new OrderedItemsWrapper(items);
    }

    private JsonObject findJsonItemWithName(JsonObject root, String name) {
        JsonArray data = root.getAsJsonArray("data");
        for (int i = 0; i < data.size(); i++) {
            JsonObject item = data.get(i).getAsJsonObject();
            if (item.get("name").getAsString().equals(name)) {
                return item;
            }
        }
        throw new JsonParseException("view element is inconsistent with data");
    }

}
