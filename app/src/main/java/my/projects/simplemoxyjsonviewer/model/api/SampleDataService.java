package my.projects.simplemoxyjsonviewer.model.api;


import com.google.gson.JsonParseException;

import java.io.IOException;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import my.projects.simplemoxyjsonviewer.model.api.response.ResponseStatus;
import my.projects.simplemoxyjsonviewer.model.api.response.SampleDataResponse;
import my.projects.simplemoxyjsonviewer.model.api.serialization.OrderedItemsWrapper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SampleDataService {

    private SampleApi api;

    public SampleDataService(SampleApi api) {
        this.api = api;
    }

    public Single<SampleDataResponse> getSampleData() {
        return new Single<SampleDataResponse>() {
            @Override
            protected void subscribeActual(SingleObserver<? super SampleDataResponse> observer) {
                api.getSampleData().enqueue(new Callback<OrderedItemsWrapper>() {
                    @Override
                    public void onResponse(Call<OrderedItemsWrapper> call, Response<OrderedItemsWrapper> response) {
                        if (!response.isSuccessful()) {
                            // server error
                            observer.onSuccess(SampleDataResponse.statusResponse(ResponseStatus.SERVER_ERROR));
                            return;
                        }
                        OrderedItemsWrapper itemsWrapper = response.body();
                        observer.onSuccess(new SampleDataResponse(itemsWrapper, ResponseStatus.OK));
                    }

                    @Override
                    public void onFailure(Call<OrderedItemsWrapper> call, Throwable error) {
                        if (error instanceof JsonParseException) {
                            observer.onSuccess(SampleDataResponse.statusResponse(ResponseStatus.DESERIALIZER_ERROR));
                        } else if (error instanceof IOException) {
                            observer.onSuccess(SampleDataResponse.statusResponse(ResponseStatus.NETWORK_ERROR));
                        } else {
                            observer.onSuccess(SampleDataResponse.statusResponse(ResponseStatus.UNKNOWN_ERROR));
                        }
                    }
                });
            }
        };
    }
}
