package my.projects.simplemoxyjsonviewer.model.api.response;

import my.projects.simplemoxyjsonviewer.model.api.serialization.OrderedItemsWrapper;

public class SampleDataResponse implements SampleApiResponse<OrderedItemsWrapper> {

    private OrderedItemsWrapper itemsWrapper;
    private ResponseStatus status;

    public SampleDataResponse(OrderedItemsWrapper itemsWrapper, ResponseStatus status) {
        this.itemsWrapper = itemsWrapper;
        this.status = status;
    }

    public static SampleDataResponse statusResponse(ResponseStatus status) {
        return new SampleDataResponse(null, status);
    }

    @Override
    public ResponseStatus getStatus() {
        return status;
    }

    @Override
    public OrderedItemsWrapper getData() {
        return itemsWrapper;
    }
}
