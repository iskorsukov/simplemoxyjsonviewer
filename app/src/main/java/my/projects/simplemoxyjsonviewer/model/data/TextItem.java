package my.projects.simplemoxyjsonviewer.model.data;

public class TextItem extends Item {

    private String text;

    public TextItem(String name, String text) {
        super(name);
        this.text = text;
    }

    @Override
    public Descriptor getDescriptor() {
        return Descriptor.TEXT;
    }

    @Override
    public String getMetaData() {
        return "name : " + name;
    }

    public String getText() {
        return text;
    }
}
