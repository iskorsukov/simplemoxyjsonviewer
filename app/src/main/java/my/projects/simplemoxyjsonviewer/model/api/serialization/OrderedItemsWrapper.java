package my.projects.simplemoxyjsonviewer.model.api.serialization;

import java.util.Collections;
import java.util.List;

import my.projects.simplemoxyjsonviewer.model.data.Item;

public class OrderedItemsWrapper {

    private List<Item> items;

    public OrderedItemsWrapper(List<Item> items) {
        this.items = items;
    }

    public List<Item> getItems() {
        return Collections.unmodifiableList(items);
    }
}
