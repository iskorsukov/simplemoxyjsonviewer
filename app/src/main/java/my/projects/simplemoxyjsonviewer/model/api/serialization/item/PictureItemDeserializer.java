package my.projects.simplemoxyjsonviewer.model.api.serialization.item;

import com.google.gson.JsonObject;

import my.projects.simplemoxyjsonviewer.model.data.PictureItem;

public class PictureItemDeserializer implements SampleItemDeserializer<PictureItem> {
    @Override
    public PictureItem deserialize(JsonObject itemRoot) {
        String name = itemRoot.get("name").getAsString();

        JsonObject data = itemRoot.getAsJsonObject("data");
        String url = data.get("url").getAsString();
        String text = data.get("text").getAsString();

        return new PictureItem(name, url, text);
    }
}
