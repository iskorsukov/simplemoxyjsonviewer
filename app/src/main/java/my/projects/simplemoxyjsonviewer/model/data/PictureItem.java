package my.projects.simplemoxyjsonviewer.model.data;

public class PictureItem extends Item {

    private final String text;
    private final String url;

    public PictureItem(String name, String url, String text) {
        super(name);
        this.text = text;
        this.url = url;
    }

    @Override
    public Descriptor getDescriptor() {
        return Descriptor.PICTURE;
    }

    @Override
    public String getMetaData() {
        return "name : " + name + ", text : " + text;
    }

    public String getText() {
        return text;
    }

    public String getUrl() {
        return url;
    }
}
