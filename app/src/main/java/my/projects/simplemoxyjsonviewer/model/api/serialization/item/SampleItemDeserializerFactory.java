package my.projects.simplemoxyjsonviewer.model.api.serialization.item;

import java.util.HashMap;
import java.util.Map;

import my.projects.simplemoxyjsonviewer.model.data.Item;

public class SampleItemDeserializerFactory {

    private Map<String, SampleItemDeserializer<? extends Item>> deserializerMap = new HashMap<>();

    public SampleItemDeserializerFactory() {
        deserializerMap.put("hz", new TextItemDeserializer());
        deserializerMap.put("picture", new PictureItemDeserializer());
        deserializerMap.put("selector", new SelectorItemDeserializer());
    }

    public SampleItemDeserializer<? extends Item> getDeserializer(String name) {
        if (!deserializerMap.containsKey(name)) {
            throw new IllegalArgumentException("There is no deserializer for " + name);
        }
        return deserializerMap.get(name);
    }
}
