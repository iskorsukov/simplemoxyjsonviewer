package my.projects.simplemoxyjsonviewer.model.api.serialization.item;

import com.google.gson.JsonObject;

public interface SampleItemDeserializer<T> {
    T deserialize(JsonObject itemRoot);
}
