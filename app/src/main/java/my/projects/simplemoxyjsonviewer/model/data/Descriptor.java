package my.projects.simplemoxyjsonviewer.model.data;

public enum Descriptor {
    TEXT, PICTURE, SELECTOR;
}
