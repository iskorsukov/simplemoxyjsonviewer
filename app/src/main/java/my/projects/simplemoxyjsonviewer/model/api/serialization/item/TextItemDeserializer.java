package my.projects.simplemoxyjsonviewer.model.api.serialization.item;

import com.google.gson.JsonObject;

import my.projects.simplemoxyjsonviewer.model.data.TextItem;

public class TextItemDeserializer implements SampleItemDeserializer<TextItem> {
    @Override
    public TextItem deserialize(JsonObject itemRoot) {
        String name = itemRoot.get("name").getAsString();

        JsonObject data = itemRoot.getAsJsonObject("data");
        String text = data.get("text").getAsString();

        return new TextItem(name, text);
    }
}
