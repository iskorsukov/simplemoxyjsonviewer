package my.projects.simplemoxyjsonviewer.model.data;

public abstract class Item {

    protected final String name;

    public Item(String name) {
        this.name = name;
    }

    public abstract Descriptor getDescriptor();

    public abstract String getMetaData();
}
