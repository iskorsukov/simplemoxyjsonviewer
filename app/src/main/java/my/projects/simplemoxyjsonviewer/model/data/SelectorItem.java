package my.projects.simplemoxyjsonviewer.model.data;

public class SelectorItem extends Item {

    // mutable
    private final Selector selector;

    public SelectorItem(String name, Selector selector) {
        super(name);
        this.selector = selector;
    }

    @Override
    public Descriptor getDescriptor() {
        return Descriptor.SELECTOR;
    }

    @Override
    public String getMetaData() {
        String metaData = "name : " + name + ", ";
        if (selector.getSelectedItemId() != -1) {
            metaData += "selected : ";
            metaData += String.valueOf(selector.getSelectedItemId());
        } else {
            metaData += "none selected";
        }
        return metaData;
    }

    public Selector getSelector() {
        return selector;
    }
}
