package my.projects.simplemoxyjsonviewer.model.api.response;

public enum ResponseStatus {
    OK, NETWORK_ERROR, SERVER_ERROR, DESERIALIZER_ERROR, UNKNOWN_ERROR;
}
