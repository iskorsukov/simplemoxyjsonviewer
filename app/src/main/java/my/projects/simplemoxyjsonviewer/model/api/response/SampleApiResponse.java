package my.projects.simplemoxyjsonviewer.model.api.response;

public interface SampleApiResponse<T> {
    ResponseStatus getStatus();
    T getData();
}
