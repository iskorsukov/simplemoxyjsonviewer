package my.projects.simplemoxyjsonviewer.model.data;

import java.util.Objects;

public class SelectorOption {

    private final long id;
    private final String text;

    public SelectorOption(long id, String text) {
        this.id = id;
        this.text = text;
    }

    public long getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SelectorOption option = (SelectorOption) o;
        return id == option.id &&
                Objects.equals(text, option.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, text);
    }
}
