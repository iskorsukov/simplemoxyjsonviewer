package my.projects.simplemoxyjsonviewer.model.data;

import android.util.LongSparseArray;

import java.util.ArrayList;
import java.util.List;

public class Selector {

    private long selectedItemId;
    private LongSparseArray<SelectorOption> options = new LongSparseArray<>();

    public Selector(long selectedItemId, List<SelectorOption> options) {
        this.selectedItemId = selectedItemId;
        for (SelectorOption option : options) {
            this.options.put(option.getId(), option);
        }
    }

    public Selector(List<SelectorOption> options) {
        this(0, options);
    }

    public long getSelectedItemId() {
        return selectedItemId;
    }

    public List<SelectorOption> getOptions() {
        ArrayList<SelectorOption> optionArrayList = new ArrayList<>(options.size());
        for (int i = 0; i < options.size(); i++) {
            optionArrayList.add(options.get(options.keyAt(i)));
        }
        return optionArrayList;
    }

    public SelectorOption getSelectedOption() {
        if (selectedItemId == -1) {
            throw new IllegalStateException("No item is selected");
        }
        return options.get(selectedItemId);
    }

    public void select(SelectorOption option) {
        if (options.indexOfKey(option.getId()) < 0) {
            throw new IllegalArgumentException("Illegal option");
        }
        selectedItemId = option.getId();
    }
}
