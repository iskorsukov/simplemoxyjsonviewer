package my.projects.simplemoxyjsonviewer.model.api.serialization.item;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import my.projects.simplemoxyjsonviewer.model.data.Selector;
import my.projects.simplemoxyjsonviewer.model.data.SelectorItem;
import my.projects.simplemoxyjsonviewer.model.data.SelectorOption;

public class SelectorItemDeserializer implements SampleItemDeserializer<SelectorItem> {
    @Override
    public SelectorItem deserialize(JsonObject itemRoot) {
        String name = itemRoot.get("name").getAsString();

        JsonObject data = itemRoot.getAsJsonObject("data");
        long selectedId = data.get("selectedId").getAsLong();

        JsonArray variants = data.getAsJsonArray("variants");

        List<SelectorOption> options = new ArrayList<>();

        for (int i = 0; i < variants.size(); i++) {
            JsonObject variant = variants.get(i).getAsJsonObject();
            long id = variant.get("id").getAsLong();
            String text = variant.get("text").getAsString();
            options.add(new SelectorOption(id, text));
        }

        Selector selector = new Selector(selectedId, options);

        return new SelectorItem(name, selector);
    }
}
