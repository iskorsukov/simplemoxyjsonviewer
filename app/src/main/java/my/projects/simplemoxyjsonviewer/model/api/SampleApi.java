package my.projects.simplemoxyjsonviewer.model.api;

import my.projects.simplemoxyjsonviewer.model.api.serialization.OrderedItemsWrapper;
import retrofit2.Call;
import retrofit2.http.GET;

public interface SampleApi {

    String ENDPOINT = "https://prnk.blob.core.windows.net/";

    @GET("tmp/JSONSample.json")
    Call<OrderedItemsWrapper> getSampleData();
}
