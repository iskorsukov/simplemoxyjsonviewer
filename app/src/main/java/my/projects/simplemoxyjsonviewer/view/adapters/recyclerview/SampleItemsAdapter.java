package my.projects.simplemoxyjsonviewer.view.adapters.recyclerview;

import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import my.projects.simplemoxyjsonviewer.model.data.Item;
import my.projects.simplemoxyjsonviewer.view.adapters.recyclerview.viewholders.SampleItemViewHolder;
import my.projects.simplemoxyjsonviewer.view.adapters.recyclerview.viewholders.SampleItemViewHolderHelper;

public class SampleItemsAdapter extends RecyclerView.Adapter<SampleItemViewHolder> {

    private SampleItemViewHolderHelper viewHolderHelper = new SampleItemViewHolderHelper();

    private List<Item> items;

    public interface SampleItemsAdapterListener {
        void onItemClicked(Item item);
    }

    private SampleItemsAdapterListener listener;

    public SampleItemsAdapter(List<Item> items, SampleItemsAdapterListener listener) {
        this.items = items;
        this.listener = listener;
    }

    @NonNull
    @Override
    public SampleItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return viewHolderHelper.createViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(@NonNull SampleItemViewHolder holder, int position) {
        // Unchecked call - relies on correctness of view holder creation for different types (SampleItemViewHolderHelper)
        holder.bind(items.get(position));
        holder.bindListener(listener);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemViewType(int position) {
        return viewHolderHelper.getViewType(items.get(position));
    }
}
