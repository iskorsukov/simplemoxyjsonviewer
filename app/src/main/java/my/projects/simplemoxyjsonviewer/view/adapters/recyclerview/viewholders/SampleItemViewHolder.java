package my.projects.simplemoxyjsonviewer.view.adapters.recyclerview.viewholders;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import my.projects.simplemoxyjsonviewer.view.adapters.recyclerview.SampleItemsAdapter;

public abstract class SampleItemViewHolder<T> extends RecyclerView.ViewHolder {

    public SampleItemViewHolder(@NonNull View itemView) {
        super(itemView);
    }

    public abstract void bind(T item);
    public abstract void bindListener(SampleItemsAdapter.SampleItemsAdapterListener listener);
}
