package my.projects.simplemoxyjsonviewer.view.state;

import java.util.EnumMap;
import java.util.Map;

import my.projects.simplemoxyjsonviewer.model.api.response.ResponseStatus;

public class ResponseStatusToStateMapper {

    private static Map<ResponseStatus, StateDescriptor> map = new EnumMap<>(ResponseStatus.class);

    static {
        map.put(ResponseStatus.NETWORK_ERROR, MainStateDescriptor.NETWORK_ERROR);
        map.put(ResponseStatus.DESERIALIZER_ERROR, MainStateDescriptor.DESERIALIZER_ERROR);
        map.put(ResponseStatus.SERVER_ERROR, MainStateDescriptor.SERVER_ERROR);
        map.put(ResponseStatus.UNKNOWN_ERROR, MainStateDescriptor.UNKNOWN_ERROR);
    }

    public static StateDescriptor map(ResponseStatus responseStatus) {
        return map.get(responseStatus);
    }
}
