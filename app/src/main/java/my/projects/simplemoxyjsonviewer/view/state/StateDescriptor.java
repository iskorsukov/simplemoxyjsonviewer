package my.projects.simplemoxyjsonviewer.view.state;

public interface StateDescriptor {
    int getMessageRes();
}
