package my.projects.simplemoxyjsonviewer.view.adapters.recyclerview.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import androidx.annotation.NonNull;
import my.projects.simplemoxyjsonviewer.R;
import my.projects.simplemoxyjsonviewer.model.data.PictureItem;
import my.projects.simplemoxyjsonviewer.view.adapters.recyclerview.SampleItemsAdapter;

public class PictureItemViewHolder extends SampleItemViewHolder<PictureItem> {

    private PictureItem item;

    private ImageView imageView;
    private TextView urlTextView;

    public PictureItemViewHolder(@NonNull View itemView) {
        super(itemView);
        imageView = itemView.findViewById(R.id.picture_item_image);
        urlTextView = itemView.findViewById(R.id.picture_item_url);
    }

    @Override
    public void bind(PictureItem item) {
        this.item = item;
        urlTextView.setText(item.getUrl());
        Glide.with(imageView)
                .load(item.getUrl())
                .placeholder(android.R.drawable.ic_menu_gallery)
                .error(android.R.drawable.ic_menu_report_image)
                .into(imageView);
    }

    @Override
    public void bindListener(final SampleItemsAdapter.SampleItemsAdapterListener listener) {
        itemView.setOnClickListener(v -> listener.onItemClicked(item));
    }
}
