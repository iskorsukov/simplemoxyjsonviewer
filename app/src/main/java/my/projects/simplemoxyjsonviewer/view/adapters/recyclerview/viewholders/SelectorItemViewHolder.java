package my.projects.simplemoxyjsonviewer.view.adapters.recyclerview.viewholders;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import my.projects.simplemoxyjsonviewer.R;
import my.projects.simplemoxyjsonviewer.model.data.SelectorItem;
import my.projects.simplemoxyjsonviewer.model.data.SelectorOption;
import my.projects.simplemoxyjsonviewer.view.adapters.recyclerview.SampleItemsAdapter;
import my.projects.simplemoxyjsonviewer.view.adapters.spinner.SelectorSpinnerAdapter;

public class SelectorItemViewHolder extends SampleItemViewHolder<SelectorItem> {

    private SelectorItem item;

    private Spinner selectorSpinner;
    private SelectorSpinnerAdapter spinnerAdapter;

    public SelectorItemViewHolder(@NonNull View itemView) {
        super(itemView);
        selectorSpinner = itemView.findViewById(R.id.selector_item_spinner);
        spinnerAdapter = new SelectorSpinnerAdapter(itemView.getContext());
    }

    @Override
    public void bind(SelectorItem item) {
        this.item = item;
        spinnerAdapter.setOptions(item.getSelector().getOptions());
        selectorSpinner.setAdapter(spinnerAdapter);
        selectorSpinner.setSelection((int) item.getSelector().getSelectedItemId());
    }

    @Override
    public void bindListener(final SampleItemsAdapter.SampleItemsAdapterListener listener) {
        selectorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            // to track first event that fires when view is built
            private boolean firstEventFired;

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!firstEventFired) {
                    // first event when view is built -> ignore
                    firstEventFired = true;
                    return;
                }
                SelectorOption option = spinnerAdapter.getItem(position);
                item.getSelector().select(option);
                listener.onItemClicked(item);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}
