package my.projects.simplemoxyjsonviewer.view.adapters.spinner;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import my.projects.simplemoxyjsonviewer.R;
import my.projects.simplemoxyjsonviewer.model.data.SelectorOption;

public class SelectorSpinnerAdapter extends ArrayAdapter<SelectorOption> {

    private List<SelectorOption> options;

    public SelectorSpinnerAdapter(@NonNull Context context) {
        super(context, R.layout.selector_option_adapter_item);
    }

    public SelectorSpinnerAdapter(@NonNull Context context, List<SelectorOption> options) {
        super(context.getApplicationContext(), R.layout.selector_option_adapter_item);
        this.options = options;
    }

    public void setOptions(List<SelectorOption> options) {
        this.options = options;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getView(options.get(position), parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getView(options.get(position), parent);
    }

    private View getView(SelectorOption option, ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.selector_option_adapter_item, parent, false);
        TextView textView = view.findViewById(R.id.selector_option_text);
        textView.setText(option.getText());
        return view;
    }

    @Override
    public int getCount() {
        return options.size();
    }

    @Nullable
    @Override
    public SelectorOption getItem(int position) {
        return options.get(position);
    }

}
