package my.projects.simplemoxyjsonviewer.view.adapters.recyclerview.viewholders;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import my.projects.simplemoxyjsonviewer.R;
import my.projects.simplemoxyjsonviewer.model.data.TextItem;
import my.projects.simplemoxyjsonviewer.view.adapters.recyclerview.SampleItemsAdapter;

public class TextItemViewHolder extends SampleItemViewHolder<TextItem> {

    private TextItem item;

    private TextView textView;

    public TextItemViewHolder(@NonNull View itemView) {
        super(itemView);
        textView = itemView.findViewById(R.id.text_item_text);
    }

    @Override
    public void bind(TextItem textItem) {
        this.item = textItem;

        textView.setText(textItem.getText());
    }

    @Override
    public void bindListener(final SampleItemsAdapter.SampleItemsAdapterListener listener) {
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClicked(item);
            }
        });
    }


}
