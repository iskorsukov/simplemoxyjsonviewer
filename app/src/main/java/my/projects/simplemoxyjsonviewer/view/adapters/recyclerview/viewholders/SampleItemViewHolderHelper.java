package my.projects.simplemoxyjsonviewer.view.adapters.recyclerview.viewholders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import my.projects.simplemoxyjsonviewer.R;
import my.projects.simplemoxyjsonviewer.model.data.Descriptor;
import my.projects.simplemoxyjsonviewer.model.data.Item;

public class SampleItemViewHolderHelper {

    public int getViewType(Item item) {
        return item.getDescriptor().ordinal();
    }

    public SampleItemViewHolder createViewHolder(ViewGroup parent, int viewType) {
        Descriptor descriptor = Descriptor.values()[viewType];
        View view;
        switch (descriptor) {
            case TEXT:
                view  = LayoutInflater.from(parent.getContext()).inflate(R.layout.text_item, parent, false);
                return new TextItemViewHolder(view);
            case PICTURE:
                view  = LayoutInflater.from(parent.getContext()).inflate(R.layout.picture_item, parent, false);
                return new PictureItemViewHolder(view);
            case SELECTOR:
                view  = LayoutInflater.from(parent.getContext()).inflate(R.layout.selector_item, parent, false);
                return new SelectorItemViewHolder(view);
            default:
                throw new IllegalArgumentException("ViewHolder is not defined for viewtype");
        }
    }
}
