package my.projects.simplemoxyjsonviewer.view.state;

import my.projects.simplemoxyjsonviewer.R;

public enum MainStateDescriptor implements StateDescriptor {
    LOADING(R.string.loading_state),
    NETWORK_ERROR(R.string.network_error_state),
    SERVER_ERROR(R.string.server_error_state),
    DESERIALIZER_ERROR(R.string.deserializer_error_state),
    UNKNOWN_ERROR(R.string.unknown_error_state);

    private int messageStringRes;

    MainStateDescriptor(int messageStringRes) {
        this.messageStringRes = messageStringRes;
    }

    @Override
    public int getMessageRes() {
        return messageStringRes;
    }
}
