package my.projects.simplemoxyjsonviewer.view.interfaces.main;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import java.util.List;

import my.projects.simplemoxyjsonviewer.model.data.Item;
import my.projects.simplemoxyjsonviewer.view.state.StateDescriptor;
import my.projects.simplemoxyjsonviewer.view.strategies.ClearByTagAndExecuteStrategy;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface MainView extends MvpView {
    void showItems(List<Item> items);

    @StateStrategyType(value = AddToEndSingleStrategy.class, tag = "metadata")
    void showMetaData(String metaData);
    @StateStrategyType(value = ClearByTagAndExecuteStrategy.class, tag = "metadata")
    void hideMetaData();

    @StateStrategyType(value = AddToEndSingleStrategy.class, tag = "state")
    void showStateMessage(StateDescriptor stateDescriptor);
    @StateStrategyType(value = ClearByTagAndExecuteStrategy.class, tag = "state")
    void hideStateMessage();
}
