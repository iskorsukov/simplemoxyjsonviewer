package my.projects.simplemoxyjsonviewer.view.activities.main;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.presenter.InjectPresenter;

import java.util.List;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import my.projects.simplemoxyjsonviewer.R;
import my.projects.simplemoxyjsonviewer.model.data.Item;
import my.projects.simplemoxyjsonviewer.presenter.MainPresenter;
import my.projects.simplemoxyjsonviewer.view.activities.base.MvpAppCompatActivity;
import my.projects.simplemoxyjsonviewer.view.adapters.recyclerview.SampleItemsAdapter;
import my.projects.simplemoxyjsonviewer.view.interfaces.main.MainView;
import my.projects.simplemoxyjsonviewer.view.state.StateDescriptor;

public class MainActivity extends MvpAppCompatActivity implements MainView, SampleItemsAdapter.SampleItemsAdapterListener, MvpView {

    @InjectPresenter
    MainPresenter presenter;

    private RecyclerView recyclerView;
    private TextView metaDataTextView;
    private TextView statusTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bindUIVariables();
        configureRecyclerView();
    }

    private void bindUIVariables() {
        recyclerView = findViewById(R.id.items_recycler_view);
        metaDataTextView = findViewById(R.id.meta_data_view);
        statusTextView = findViewById(R.id.status_text_view);
    }

    private void configureRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
    }

    @Override
    public void showItems(List<Item> items) {
        SampleItemsAdapter adapter = new SampleItemsAdapter(items, this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showStateMessage(StateDescriptor stateDescriptor) {
        statusTextView.setText(stateDescriptor.getMessageRes());
        statusTextView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideStateMessage() {
        statusTextView.setVisibility(View.GONE);
    }

    @Override
    public void showMetaData(String metaData) {
        metaDataTextView.setVisibility(View.VISIBLE);
        metaDataTextView.setText(metaData);
    }

    @Override
    public void hideMetaData() {
        metaDataTextView.setVisibility(View.GONE);
    }

    @Override
    public void onItemClicked(Item item) {
        presenter.onItemClicked(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.main_menu_refresh:
                presenter.onRefreshMenuItemClicked();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
