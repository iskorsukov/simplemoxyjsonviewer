package my.projects.simplemoxyjsonviewer.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import my.projects.simplemoxyjsonviewer.App;
import my.projects.simplemoxyjsonviewer.model.api.SampleDataService;
import my.projects.simplemoxyjsonviewer.model.api.response.ResponseStatus;
import my.projects.simplemoxyjsonviewer.model.data.Item;
import my.projects.simplemoxyjsonviewer.view.interfaces.main.MainView;
import my.projects.simplemoxyjsonviewer.view.state.MainStateDescriptor;
import my.projects.simplemoxyjsonviewer.view.state.ResponseStatusToStateMapper;
import my.projects.simplemoxyjsonviewer.view.state.StateDescriptor;

@InjectViewState
public class MainPresenter extends MvpPresenter<MainView> {

    @Inject
    SampleDataService sampleDataService;

    private Disposable apiCallDisposable;

    public MainPresenter() {
        App.getAppComponent().inject(this);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        loadSampleData();
    }

    private void loadSampleData() {
        getViewState().hideMetaData(); // hide old metadata in case dataset changed
        getViewState().showStateMessage(MainStateDescriptor.LOADING);
        apiCallDisposable = sampleDataService.getSampleData()
                .subscribe(sampleDataResponse -> {
                    if (sampleDataResponse.getStatus() == ResponseStatus.OK) {
                        getViewState().showItems(sampleDataResponse.getData().getItems());
                        getViewState().hideStateMessage();
                    } else {
                        StateDescriptor stateDescriptor = ResponseStatusToStateMapper.map(sampleDataResponse.getStatus());
                        getViewState().showStateMessage(stateDescriptor);
                    }
                    // subscription will get disposed of automatically because of Single implementation
                }, error -> {
                    // unexpected error that was not caught in SampleDataService
                    error.printStackTrace();
                    getViewState().showStateMessage(MainStateDescriptor.UNKNOWN_ERROR);
                });
    }

    public void onItemClicked(Item item) {
        getViewState().showMetaData(item.getMetaData());
    }

    public void onRefreshMenuItemClicked() {
        if (apiCallDisposable == null || apiCallDisposable.isDisposed()) {
            // no ongoing subscription
            loadSampleData();
        }
    }

    @Override
    public void onDestroy() {
        if (apiCallDisposable != null) {
            // dispose of an ongoing subscription
            apiCallDisposable.dispose();
        }
        super.onDestroy();
    }
}
