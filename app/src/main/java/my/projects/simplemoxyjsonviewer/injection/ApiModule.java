package my.projects.simplemoxyjsonviewer.injection;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import my.projects.simplemoxyjsonviewer.model.api.SampleApi;
import my.projects.simplemoxyjsonviewer.model.api.SampleDataService;
import my.projects.simplemoxyjsonviewer.model.api.serialization.OrderedItemsWrapper;
import my.projects.simplemoxyjsonviewer.model.api.serialization.SampleDataDeserializer;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApiModule {

    @Provides
    @Singleton
    public SampleApi providesSampleApi() {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(OrderedItemsWrapper.class, new SampleDataDeserializer())
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(SampleApi.ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit.create(SampleApi.class);
    }

    @Provides
    @Singleton
    public SampleDataService providesSampleDataService(SampleApi api) {
        return new SampleDataService(api);
    }
}
