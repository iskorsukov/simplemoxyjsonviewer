package my.projects.simplemoxyjsonviewer.injection;

import javax.inject.Singleton;

import dagger.Component;
import my.projects.simplemoxyjsonviewer.model.api.SampleApi;
import my.projects.simplemoxyjsonviewer.model.api.SampleDataService;
import my.projects.simplemoxyjsonviewer.presenter.MainPresenter;

@Component(modules = {ApiModule.class})
@Singleton
public interface AppComponent {
    SampleApi sampleApi();
    SampleDataService sampleDataService();

    void inject(MainPresenter presenter);
}
