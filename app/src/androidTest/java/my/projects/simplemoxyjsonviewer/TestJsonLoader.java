package my.projects.simplemoxyjsonviewer;

import java.util.Scanner;

public class TestJsonLoader {

    public String loadJson(String filename) {
        ClassLoader cl = getClass().getClassLoader();
        if (cl == null) {
            throw new RuntimeException("Could not get class loader");
        }

        try (Scanner scanner = new Scanner(cl.getResourceAsStream(filename))) {
            StringBuilder jsonBuilder = new StringBuilder();
            String input;
            while (scanner.hasNextLine() && (input = scanner.nextLine()) != null) {
                jsonBuilder.append(input);
            }
            return jsonBuilder.toString();
        }
    }
}
