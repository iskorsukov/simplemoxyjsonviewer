package my.projects.simplemoxyjsonviewer.view.activities.main;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import androidx.test.rule.ActivityTestRule;
import my.projects.simplemoxyjsonviewer.R;
import my.projects.simplemoxyjsonviewer.model.data.Item;
import my.projects.simplemoxyjsonviewer.model.data.TextItem;
import my.projects.simplemoxyjsonviewer.presenter.MainPresenter;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.AllOf.allOf;
import static org.mockito.Mockito.verify;

public class MainActivityToPresenterUITest {

    @Rule
    public ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule<>(MainActivity.class);

    private MainPresenter mockPresenter;

    @Before
    public void setupMockPresenter() {
        mockPresenter = Mockito.mock(MainPresenter.class);
        activityTestRule.getActivity().presenter = mockPresenter;
    }

    @Test
    public void clickOnRefreshMenuItemPropagatesToPresenter() {
        clickOnRefreshMenuItem();

        verify(mockPresenter).onRefreshMenuItemClicked();
    }

    private void clickOnRefreshMenuItem() {
        onView(withId(R.id.main_menu_refresh)).perform(click());
    }

    @Test
    public void clickOnItemPropagatesToPresenter() throws Throwable {
        List<Item> items = prepareTestItems();
        pushItemsToActivity(items);

        Item clickedItem = clickOnRandomItem(items);

        verify(mockPresenter).onItemClicked(ArgumentMatchers.eq(clickedItem));
    }

    private List<Item> prepareTestItems() {
        List<Item> items = new ArrayList<>();
        items.add(new TextItem("text", "text"));
        items.add(new TextItem("second text", "second text"));
        items.add(new TextItem("third text", "third text"));

        return items;
    }

    private void pushItemsToActivity(final List<Item> items) throws Throwable {
        activityTestRule.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                activityTestRule.getActivity().showItems(items);
            }
        });
    }

    private Item clickOnRandomItem(List<Item> items) {
        int randPos = new Random().nextInt(items.size());
        Item clickedItem = items.get(randPos);

        onView(allOf(withId(R.id.text_item_text), withText(((TextItem) clickedItem).getText()))).perform(click());

        return clickedItem;
    }
}
