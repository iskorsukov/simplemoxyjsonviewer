package my.projects.simplemoxyjsonviewer.view.activities.main;

import android.content.Intent;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.io.IOException;

import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.rule.ActivityTestRule;
import my.projects.simplemoxyjsonviewer.App;
import my.projects.simplemoxyjsonviewer.R;
import my.projects.simplemoxyjsonviewer.RecyclerViewMatcher;
import my.projects.simplemoxyjsonviewer.SpinnerViewMatcher;
import my.projects.simplemoxyjsonviewer.TestJsonLoader;
import my.projects.simplemoxyjsonviewer.injection.DaggerTestAppComponent;
import my.projects.simplemoxyjsonviewer.injection.TestApiModule;
import my.projects.simplemoxyjsonviewer.injection.TestAppComponent;
import my.projects.simplemoxyjsonviewer.model.api.SampleApi;
import my.projects.simplemoxyjsonviewer.model.api.serialization.OrderedItemsWrapper;
import my.projects.simplemoxyjsonviewer.model.api.serialization.SampleDataDeserializer;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

public class MainActivityUITest {

    @Rule
    public ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule<>(MainActivity.class, false, false);

    private MockWebServer mockWebServer;

    @Before
    public void setupMockWebServer() {
        mockWebServer = new MockWebServer();
        String endpoint = mockWebServer.url("/").toString();

        SampleApi sampleApi = buildSampleApi(endpoint);

        TestAppComponent appComponent = DaggerTestAppComponent.builder()
                .testApiModule(new TestApiModule(sampleApi)).build();
        App.setAppComponent(appComponent);
    }

    private SampleApi buildSampleApi(String endpoint) {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(OrderedItemsWrapper.class, new SampleDataDeserializer())
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(endpoint)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit.create(SampleApi.class);
    }

    private void launchActivity() {
        activityTestRule.launchActivity(new Intent());
    }

    @Test
    public void showsLoadingStatusWhenLoadingData() {
        launchActivity();
        // activity gets attached to presenter and it requests data after activity launch

        assertShowsLoadingStatus();
    }

    private void assertShowsLoadingStatus() {
        onView(withId(R.id.status_text_view)).check(matches(isDisplayed()));
        onView(withId(R.id.status_text_view)).check(matches(withText(R.string.loading_state)));
    }

    @Test
    public void showsLoadingStatusWhenLoadingDataAfterClickOnRefresh() {
        prepareTestDataResponse();
        launchActivity();

        clickOnRefreshMenuItem();

        assertShowsLoadingStatus();
    }

    private void prepareTestDataResponse() {
        TestJsonLoader jsonLoader = new TestJsonLoader();
        String json = jsonLoader.loadJson("sample_data.json");

        mockWebServer.enqueue(new MockResponse().setResponseCode(200).setBody(json));
    }

    private void clickOnRefreshMenuItem() {
        onView(withId(R.id.main_menu_refresh)).perform(click());
    }

    @Test
    public void showsNetworkErrorStatusWhenCantConnectToServer() throws IOException {
        mockWebServer.close();
        launchActivity();

        assertShowsNetworkErrorStatus();
    }

    private void assertShowsNetworkErrorStatus() {
        onView(withId(R.id.status_text_view)).check(matches(isDisplayed()));
        onView(withId(R.id.status_text_view)).check(matches(withText(R.string.network_error_state)));
    }

    @Test
    public void showsServerErrorOnBadServerResponse() {
        prepare404ServerResponse();
        launchActivity();

        assertShowsServerErrorStatus();
    }

    private void prepare404ServerResponse() {
        mockWebServer.enqueue(new MockResponse().setResponseCode(404));
    }

    private void assertShowsServerErrorStatus() {
        onView(withId(R.id.status_text_view)).check(matches(isDisplayed()));
        onView(withId(R.id.status_text_view)).check(matches(withText(R.string.server_error_state)));
    }

    @Test
    public void showsDeserializerErrorOnUnknownNameInJson() {
        prepareUnknownNameJsonResponse();
        launchActivity();

        assertShowsDeserializerError();
    }

    private void prepareUnknownNameJsonResponse() {
        TestJsonLoader jsonLoader = new TestJsonLoader();
        String unknownNameJson = jsonLoader.loadJson("sample_data_unknown_name.json");

        mockWebServer.enqueue(new MockResponse().setResponseCode(200).setBody(unknownNameJson));
    }

    private void assertShowsDeserializerError() {
        onView(withId(R.id.status_text_view)).check(matches(isDisplayed()));
        onView(withId(R.id.status_text_view)).check(matches(withText(R.string.deserializer_error_state)));
    }

    @Test
    public void showsDeserializerErrorOnMalformedJson() {
        prepareMalformedJsonResponse();
        launchActivity();

        assertShowsDeserializerError();
    }

    private void prepareMalformedJsonResponse() {
        TestJsonLoader jsonLoader = new TestJsonLoader();
        // prepare truncated json
        String malformedJson = jsonLoader.loadJson("sample_data.json").substring(0, 10);

        mockWebServer.enqueue(new MockResponse().setResponseCode(200).setBody(malformedJson));
    }

    @Test
    public void showsItems() {
        prepareTestDataResponse();
        launchActivity();

        assertShowsItems();
    }

    private void assertShowsItems() {
        // TextView in first position
        onView(RecyclerViewMatcher.atPositionInRecyclerView(0, R.id.items_recycler_view)).check(matches(hasDescendant(withText("тринитротолуол"))));

        // SelectorView in second position
        onView(RecyclerViewMatcher.atPositionInRecyclerView(1, R.id.items_recycler_view)).check(matches(hasDescendant(SpinnerViewMatcher.selectedSpinnerItemContains("Вариант 2"))));

        // PictureView in third position
        onView(RecyclerViewMatcher.atPositionInRecyclerView(2, R.id.items_recycler_view)).check(matches(hasDescendant(withText("https://cdnbeta.pryaniky.com/content/img/opportunities_8_talents.png"))));

        // TextView in fourth position
        onView(RecyclerViewMatcher.atPositionInRecyclerView(3, R.id.items_recycler_view)).check(matches(hasDescendant(withText("тринитротолуол"))));
    }

    @Test
    public void metadataViewIsGoneOnStart() {
        launchActivity();

        assertMetaDataViewVisibilityGone();
    }

    private void assertMetaDataViewVisibilityGone() {
        onView(withId(R.id.meta_data_view)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
    }

    @Test
    public void showsMetaDataOnItemClick() {
        prepareTestDataResponse();
        launchActivity();

        clickOnFirstItem();

        assertShowsMetaData();
    }

    private void clickOnFirstItem() {
        onView(RecyclerViewMatcher.atPositionInRecyclerView(0, R.id.items_recycler_view)).perform(click());
    }

    private void assertShowsMetaData() {
        onView(withId(R.id.meta_data_view)).check(matches(isDisplayed()));
        onView(withId(R.id.meta_data_view)).check(matches(withText("name : hz")));
    }

    @Test
    public void hidesMetaDataOnRefresh() {
        prepareTestDataResponse();
        launchActivity();

        clickOnRefreshMenuItem();

        assertMetaDataViewVisibilityGone();
    }
}
