package my.projects.simplemoxyjsonviewer.view.adapters.recyclerview.viewholders;

import android.view.ViewGroup;

import org.junit.Rule;
import org.junit.Test;

import androidx.test.rule.ActivityTestRule;
import my.projects.simplemoxyjsonviewer.R;
import my.projects.simplemoxyjsonviewer.model.data.Descriptor;
import my.projects.simplemoxyjsonviewer.model.data.Item;
import my.projects.simplemoxyjsonviewer.model.data.PictureItem;
import my.projects.simplemoxyjsonviewer.model.data.SelectorItem;
import my.projects.simplemoxyjsonviewer.model.data.TextItem;
import my.projects.simplemoxyjsonviewer.view.activities.main.MainActivity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SampleItemViewHolderHelperTest {

    @Rule
    public ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule<>(MainActivity.class);

    private SampleItemViewHolderHelper helper = new SampleItemViewHolderHelper();

    @Test
    public void getsDescriptorOrdinalAsViewType() {
        for (Descriptor descriptor : Descriptor.values()) {
            int viewType = helper.getViewType(new DescriptorItem("test", descriptor));
            assertEquals(descriptor.ordinal(), viewType);
        }
    }

    @Test
    public void createsCorrectViewHolderForItem() throws Throwable {
        ViewGroup parent = activityTestRule.getActivity().findViewById(R.id.items_recycler_view);

        // run on ui thread because on some Android API versions inflating resources must be done on ui thread
        activityTestRule.runOnUiThread(() -> {
            SampleItemViewHolder viewHolder;

            Item textItem = new TextItem("text", "text");
            viewHolder = helper.createViewHolder(parent, helper.getViewType(textItem));
            assertTrue(viewHolder instanceof TextItemViewHolder);

            Item pictureItem = new PictureItem("picture", "url", "text");
            viewHolder = helper.createViewHolder(parent, helper.getViewType(pictureItem));
            assertTrue(viewHolder instanceof PictureItemViewHolder);

            Item selectorItem = new SelectorItem("selector", null);
            viewHolder = helper.createViewHolder(parent, helper.getViewType(selectorItem));
            assertTrue(viewHolder instanceof SelectorItemViewHolder);
        });
    }

    private static class DescriptorItem extends Item {

        private Descriptor descriptor;

        public DescriptorItem(String name, Descriptor descriptor) {
            super(name);
            this.descriptor = descriptor;
        }

        @Override
        public Descriptor getDescriptor() {
            return descriptor;
        }

        @Override
        public String getMetaData() {
            return null;
        }
    }
}
