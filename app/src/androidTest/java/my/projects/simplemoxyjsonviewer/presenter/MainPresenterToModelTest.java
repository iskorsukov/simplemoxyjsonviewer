package my.projects.simplemoxyjsonviewer.presenter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import my.projects.simplemoxyjsonviewer.App;
import my.projects.simplemoxyjsonviewer.injection.DaggerTestAppComponent;
import my.projects.simplemoxyjsonviewer.injection.TestApiModule;
import my.projects.simplemoxyjsonviewer.injection.TestAppComponent;
import my.projects.simplemoxyjsonviewer.model.api.SampleApi;
import retrofit2.Call;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MainPresenterToModelTest {

    private MainPresenter presenter;
    private SampleApi mockSampleApi;

    @Before
    public void setUpTestComponent() {
        mockSampleApi = Mockito.mock(SampleApi.class);

        TestAppComponent testAppComponent = DaggerTestAppComponent.builder()
                .testApiModule(new TestApiModule(mockSampleApi)).build();
        App.setAppComponent(testAppComponent);

        presenter = new MainPresenter();
    }

    @Test
    public void buildTest() {

    }

    @Test
    public void callsApiToGetSampleDataWhenAttached() {
        prepareMockApiResponse();

        presenter.onFirstViewAttach();

        verify(mockSampleApi).getSampleData();
    }

    private void prepareMockApiResponse() {
        when(mockSampleApi.getSampleData()).thenReturn(Mockito.mock(Call.class));
    }

    @Test
    public void callsApiToGetSampleDataWhenRefreshMenuItemTriggered() {
        prepareMockApiResponse();

        presenter.onRefreshMenuItemClicked();

        verify(mockSampleApi).getSampleData();
    }
}
