package my.projects.simplemoxyjsonviewer.presenter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.util.Collections;

import io.reactivex.Single;
import my.projects.simplemoxyjsonviewer.App;
import my.projects.simplemoxyjsonviewer.injection.DaggerTestAppComponent;
import my.projects.simplemoxyjsonviewer.injection.TestApiModule;
import my.projects.simplemoxyjsonviewer.injection.TestAppComponent;
import my.projects.simplemoxyjsonviewer.model.api.SampleDataService;
import my.projects.simplemoxyjsonviewer.model.api.response.ResponseStatus;
import my.projects.simplemoxyjsonviewer.model.api.response.SampleDataResponse;
import my.projects.simplemoxyjsonviewer.model.api.serialization.OrderedItemsWrapper;
import my.projects.simplemoxyjsonviewer.view.interfaces.main.MainView;
import my.projects.simplemoxyjsonviewer.view.interfaces.main.MainView$$State;
import my.projects.simplemoxyjsonviewer.view.state.MainStateDescriptor;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MainPresenterToMainViewTest {

    private SampleDataService mockDataService;

    private MainView mockMainView;
    private MainView$$State mockMainViewState;

    private MainPresenter presenter;

    @Before
    public void setup() {
        mockMainView = Mockito.mock(MainView.class);
        mockMainViewState = Mockito.mock(MainView$$State.class);

        mockDataService = Mockito.mock(SampleDataService.class);

        TestAppComponent testAppComponent = DaggerTestAppComponent.builder()
                .testApiModule(new TestApiModule(mockDataService)).build();
        App.setAppComponent(testAppComponent);

        presenter = new MainPresenter();
        presenter.setViewState(mockMainViewState);
    }

    @Test
    public void callsShowItemsOnReceivedItems() {
        prepareEmptyTestData();

        presenter.attachView(mockMainView);
        // calls loadSampleData in onFirstViewAttach

        verify(mockMainViewState).showItems(ArgumentMatchers.anyList());
    }

    private void prepareEmptyTestData() {
        when(mockDataService.getSampleData()).thenReturn(Single.just(new SampleDataResponse(new OrderedItemsWrapper(Collections.emptyList()), ResponseStatus.OK)));
    }

    @Test
    public void callsShowLoadingStateWhenLoadingData() {
        prepareLoadingResponse();

        presenter.attachView(mockMainView);

        verify(mockMainViewState).showStateMessage(ArgumentMatchers.eq(MainStateDescriptor.LOADING));
    }

    private void prepareLoadingResponse() {
        when(mockDataService.getSampleData()).thenReturn(Single.never());
    }

    @Test
    public void callsHideStateMessageOnDataLoaded() {
        prepareEmptyTestData();

        presenter.attachView(mockMainView);

        verify(mockMainViewState).hideStateMessage();
    }

    @Test
    public void callsShowNetworkErrorStateOnNetworkError() {
        prepareStatusResponse(ResponseStatus.NETWORK_ERROR);

        presenter.attachView(mockMainView);

        verify(mockMainViewState).showStateMessage(ArgumentMatchers.eq(MainStateDescriptor.NETWORK_ERROR));
    }

    private void prepareStatusResponse(ResponseStatus status) {
        when(mockDataService.getSampleData()).thenReturn(Single.just(SampleDataResponse.statusResponse(status)));
    }

    @Test
    public void callsShowServerErrorStateOnServerError() {
        prepareStatusResponse(ResponseStatus.SERVER_ERROR);

        presenter.attachView(mockMainView);

        verify(mockMainViewState).showStateMessage(ArgumentMatchers.eq(MainStateDescriptor.SERVER_ERROR));
    }

    @Test
    public void callsShowDeserializerErrorStateOnDeserializerError() {
        prepareStatusResponse(ResponseStatus.DESERIALIZER_ERROR);

        presenter.attachView(mockMainView);

        verify(mockMainViewState).showStateMessage(ArgumentMatchers.eq(MainStateDescriptor.DESERIALIZER_ERROR));
    }
}
