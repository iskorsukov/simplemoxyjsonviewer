package my.projects.simplemoxyjsonviewer.injection;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = TestApiModule.class)
@Singleton
public interface TestAppComponent extends AppComponent {
}
