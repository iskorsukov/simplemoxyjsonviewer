package my.projects.simplemoxyjsonviewer.injection;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import my.projects.simplemoxyjsonviewer.model.api.SampleApi;
import my.projects.simplemoxyjsonviewer.model.api.SampleDataService;

@Module
public class TestApiModule {

    private SampleApi mockSampleApi;
    private SampleDataService mockDataService;

    public TestApiModule(SampleApi mockSampleApi) {
        this.mockSampleApi = mockSampleApi;
    }

    public TestApiModule(SampleDataService mockDataService) {
        this.mockDataService = mockDataService;
    }

    public TestApiModule(SampleApi mockSampleApi, SampleDataService mockDataService) {
        this.mockSampleApi = mockSampleApi;
        this.mockDataService = mockDataService;
    }

    @Provides
    @Singleton
    public SampleApi providesMockSampleApi() {
        return mockSampleApi;
    }

    @Provides
    @Singleton
    public SampleDataService providesSampleDataService() {
        if (mockDataService == null) {
            return new SampleDataService(mockSampleApi);
        } else {
            return mockDataService;
        }
    }
}
