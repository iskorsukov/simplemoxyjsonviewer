package my.projects.simplemoxyjsonviewer;

import android.view.View;
import android.widget.Spinner;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import java.util.ArrayList;

public class SpinnerViewMatcher {

    public static Matcher<View> selectedSpinnerItemContains(String text) {
        return new TypeSafeMatcher<View>() {
            @Override
            protected boolean matchesSafely(View item) {
                if (!(item instanceof Spinner)) {
                    return false;
                }
                Spinner spinner = (Spinner) item;
                View childView = spinner.getSelectedView();
                ArrayList<View> viewsWithText = new ArrayList<>();
                childView.findViewsWithText(viewsWithText, text, View.FIND_VIEWS_WITH_TEXT);

                return viewsWithText.size() > 0;
            }

            @Override
            public void describeTo(Description description) {

            }
        };
    }
}
