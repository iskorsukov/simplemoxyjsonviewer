package my.projects.simplemoxyjsonviewer.model.api.serialization;

import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import my.projects.simplemoxyjsonviewer.TestJsonLoader;
import my.projects.simplemoxyjsonviewer.model.data.Descriptor;
import my.projects.simplemoxyjsonviewer.model.data.Item;
import my.projects.simplemoxyjsonviewer.model.data.PictureItem;
import my.projects.simplemoxyjsonviewer.model.data.Selector;
import my.projects.simplemoxyjsonviewer.model.data.SelectorItem;
import my.projects.simplemoxyjsonviewer.model.data.SelectorOption;
import my.projects.simplemoxyjsonviewer.model.data.TextItem;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;

public class SampleDataDeserializerTest {

    private SampleDataDeserializer deserializer;
    private TestJsonLoader jsonLoader;

    @Before
    public void setUp() {
        deserializer = new SampleDataDeserializer();
        jsonLoader = new TestJsonLoader();
    }

    @Test
    public void deserializesSampleData() {
        JsonElement json = new JsonParser().parse(jsonLoader.loadJson("sample_data.json"));

        OrderedItemsWrapper itemsWrapper = deserializer.deserialize(json, OrderedItemsWrapper.class, null);

        assertItemDeserializedCorrectly(itemsWrapper);
    }

    private void assertItemDeserializedCorrectly(OrderedItemsWrapper orderedItemsWrapper) {
        assertNotNull(orderedItemsWrapper);
        assertNotNull(orderedItemsWrapper.getItems());
        assertEquals(4, orderedItemsWrapper.getItems().size());

        assertItemsOrderIsCorrect(orderedItemsWrapper.getItems());
        assertItemsCorrect(orderedItemsWrapper.getItems());
    }

    private void assertItemsOrderIsCorrect(List<Item> items) {
        Descriptor[] order = new Descriptor[] {Descriptor.TEXT, Descriptor.SELECTOR, Descriptor.PICTURE, Descriptor.TEXT};
        for (int i = 0; i < items.size(); i++) {
            Item item = items.get(i);
            assertEquals(order[i], item.getDescriptor());
        }
    }

    private void assertItemsCorrect(List<Item> items) {
        TextItem first = (TextItem) items.get(0);
        assertEquals("тринитротолуол", first.getText());

        SelectorItem second = (SelectorItem) items.get(1);

        Selector selector = second.getSelector();
        assertNotNull(selector);
        assertEquals(1L, selector.getSelectedItemId());

        List<SelectorOption> options = selector.getOptions();
        assertNotNull(options);
        assertEquals(3, options.size());
        assertEquals("Вариант 1", options.get(0).getText());
        assertEquals("Вариант 2", options.get(1).getText());
        assertEquals("Вариант 777", options.get(2).getText());

        PictureItem third = (PictureItem) items.get(2);
        assertEquals("https://cdnbeta.pryaniky.com/content/img/opportunities_8_talents.png", third.getUrl());

        TextItem fourth = (TextItem) items.get(0);
        assertEquals(first, fourth);
    }

    @Test(expected = JsonParseException.class)
    public void throwsExceptionOnUnknownName() {
        JsonElement json = new JsonParser().parse(jsonLoader.loadJson("sample_data_unknown_name.json"));

        deserializer.deserialize(json, OrderedItemsWrapper.class, null);
    }

    @Test(expected = JsonParseException.class)
    public void throwsExceptionOnMalformedJson() {
        JsonElement json = new JsonParser().parse(jsonLoader.loadJson("sample_data_unknown_name.json").substring(0, 10));

        deserializer.deserialize(json, OrderedItemsWrapper.class, null);

    }
}
