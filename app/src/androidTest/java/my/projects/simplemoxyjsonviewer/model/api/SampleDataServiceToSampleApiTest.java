package my.projects.simplemoxyjsonviewer.model.api;

import com.google.gson.JsonParseException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

import my.projects.simplemoxyjsonviewer.model.api.response.ResponseStatus;
import my.projects.simplemoxyjsonviewer.model.api.response.SampleDataResponse;
import my.projects.simplemoxyjsonviewer.model.api.serialization.OrderedItemsWrapper;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class SampleDataServiceToSampleApiTest {

    private SampleApi mockSampleApi;
    private SampleDataService service;

    @Before
    public void setupSampleDataServiceWithMockApi() {
        mockSampleApi = Mockito.mock(SampleApi.class);
        service = new SampleDataService(mockSampleApi);
    }

    @Test
    public void returnsOkItemsResponseOnSuccessfulApiCall() {
        prepareSuccessfulApiCall();

        SampleDataResponse response = service.getSampleData().timeout(2, TimeUnit.SECONDS).blockingGet();

        assertOkItemsResponse(response);
    }

    private void prepareSuccessfulApiCall() {
        OrderedItemsWrapper wrapper = new OrderedItemsWrapper(Collections.emptyList());
        SampleDataResponse desiredResponse = new SampleDataResponse(wrapper, ResponseStatus.OK);
        when(mockSampleApi.getSampleData()).thenReturn(new EnqueueableSampleDataCall(desiredResponse));

    }

    private void assertOkItemsResponse(SampleDataResponse response) {
        assertNotNull(response);
        assertEquals(ResponseStatus.OK, response.getStatus());
        assertNotNull(response.getData());
        assertEquals(0, response.getData().getItems().size());
    }

    @Test
    public void returnsNetworkErrorResponseOnFailureWithIOException() {
        prepareFailureWithIOExceptionApiCall();

        SampleDataResponse response = service.getSampleData().timeout(2, TimeUnit.SECONDS).blockingGet();

        assertNetworkErrorResponse(response);
    }

    private void prepareFailureWithIOExceptionApiCall() {
        when(mockSampleApi.getSampleData()).thenReturn(new EnqueueableSampleDataCall(SampleDataResponse.statusResponse(ResponseStatus.NETWORK_ERROR)));
    }

    private void assertNetworkErrorResponse(SampleDataResponse response) {
        assertNotNull(response);
        assertEquals(ResponseStatus.NETWORK_ERROR, response.getStatus());
    }

    @Test
    public void returnsServerErrorResponseOnResponseWithBadResponseCode() {
        prepareBadResponseCodeApiCall();

        SampleDataResponse response = service.getSampleData().timeout(2, TimeUnit.SECONDS).blockingGet();

        assertServerErrorResponse(response);
    }

    private void prepareBadResponseCodeApiCall() {
        when(mockSampleApi.getSampleData()).thenReturn(new EnqueueableSampleDataCall(SampleDataResponse.statusResponse(ResponseStatus.SERVER_ERROR)));
    }

    private void assertServerErrorResponse(SampleDataResponse response) {
        assertNotNull(response);
        assertEquals(ResponseStatus.SERVER_ERROR, response.getStatus());
    }

    @Test
    public void returnsDeserializerErrorResponseOnFailureWithParseJsonException() {
        prepareFailureWithJsonParseExceptionApiCall();

        SampleDataResponse response = service.getSampleData().timeout(2, TimeUnit.SECONDS).blockingGet();

        assertDeserializerErrorResponse(response);
    }

    private void prepareFailureWithJsonParseExceptionApiCall() {
        when(mockSampleApi.getSampleData()).thenReturn(new EnqueueableSampleDataCall(SampleDataResponse.statusResponse(ResponseStatus.DESERIALIZER_ERROR)));
    }

    private void assertDeserializerErrorResponse(SampleDataResponse response) {
        assertNotNull(response);
        assertEquals(ResponseStatus.DESERIALIZER_ERROR, response.getStatus());
    }

    @Test
    public void returnsUnknownErrorOnFailureWithUnexpectedException() {
        prepareFailureWithIllegalStateExceptionApiCall();

        SampleDataResponse response = service.getSampleData().timeout(2, TimeUnit.SECONDS).blockingGet();

        assertUnknownErrorResponse(response);
    }

    private void prepareFailureWithIllegalStateExceptionApiCall() {
        when(mockSampleApi.getSampleData()).thenReturn(new EnqueueableSampleDataCall(SampleDataResponse.statusResponse(ResponseStatus.UNKNOWN_ERROR)));
    }

    private void assertUnknownErrorResponse(SampleDataResponse response) {
        assertNotNull(response);
        assertEquals(ResponseStatus.UNKNOWN_ERROR, response.getStatus());
    }

    private static class EnqueueableSampleDataCall implements Call<OrderedItemsWrapper> {

        private SampleDataResponse desiredResponse;

        public EnqueueableSampleDataCall(SampleDataResponse response) {
            desiredResponse = response;
        }

        @Override
        public Response<OrderedItemsWrapper> execute() throws IOException {
            return null;
        }

        @Override
        public void enqueue(Callback<OrderedItemsWrapper> callback) {
            Response<OrderedItemsWrapper> response;
            switch (desiredResponse.getStatus()) {
                case OK:
                    response = Response.success(desiredResponse.getData());
                    callback.onResponse(this, response);
                    break;
                case NETWORK_ERROR:
                    callback.onFailure(this, new IOException());
                    break;
                case SERVER_ERROR:
                    response = Response.error(404, ResponseBody.create(MediaType.get("application/json"), ""));
                    callback.onResponse(this, response);
                    break;
                case DESERIALIZER_ERROR:
                    callback.onFailure(this, new JsonParseException("Failed to parse json"));
                    break;
                case UNKNOWN_ERROR:
                    callback.onFailure(this, new IllegalStateException("Some unknown error"));
                    break;
            }
        }

        @Override
        public boolean isExecuted() {
            return false;
        }

        @Override
        public void cancel() {

        }

        @Override
        public boolean isCanceled() {
            return false;
        }

        @Override
        public Call<OrderedItemsWrapper> clone() {
            return null;
        }

        @Override
        public Request request() {
            return null;
        }
    }
}
