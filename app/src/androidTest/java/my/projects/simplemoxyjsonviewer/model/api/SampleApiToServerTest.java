package my.projects.simplemoxyjsonviewer.model.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import my.projects.simplemoxyjsonviewer.TestJsonLoader;
import my.projects.simplemoxyjsonviewer.model.api.serialization.OrderedItemsWrapper;
import my.projects.simplemoxyjsonviewer.model.api.serialization.SampleDataDeserializer;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertNotNull;

public class SampleApiToServerTest {

    private SampleApi sampleApi;
    private MockWebServer mockWebServer;

    @Before
    public void setupSampleApiWithMockWebServer() {
        mockWebServer = new MockWebServer();
        String endpoint = mockWebServer.url("/").toString();

        sampleApi = buildSampleApi(endpoint);
    }

    private SampleApi buildSampleApi(String endpoint) {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(OrderedItemsWrapper.class, new SampleDataDeserializer())
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(endpoint)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit.create(SampleApi.class);
    }

    @Test
    public void returnsItemsOnSuccessfulResponse() throws IOException {
        prepareSuccessfulResponse();

        Call<OrderedItemsWrapper> call = sampleApi.getSampleData();
        Response<OrderedItemsWrapper> response = call.execute();
        OrderedItemsWrapper itemsWrapper = response.body();

        assertNotNull(itemsWrapper);
        assertNotNull(itemsWrapper.getItems());
        assertEquals(4, itemsWrapper.getItems().size());
    }

    private void prepareSuccessfulResponse() {
        TestJsonLoader jsonLoader = new TestJsonLoader();
        String json = jsonLoader.loadJson("sample_data.json");

        mockWebServer.enqueue(new MockResponse().setResponseCode(200).setBody(json));
    }

    @Test(expected = IOException.class)
    public void failsWithIOExceptionOnNetworkError() throws IOException {
        mockWebServer.close();

        Call<OrderedItemsWrapper> call = sampleApi.getSampleData();
        call.execute();
    }

    @Test
    public void processesBadResponseCode() throws IOException {
        prepare404ServerResponse();

        Call<OrderedItemsWrapper> call = sampleApi.getSampleData();
        Response<OrderedItemsWrapper> response = call.execute();

        assertNotNull(response);
        assertFalse(response.isSuccessful());
        assertEquals(404, response.code());
    }

    private void prepare404ServerResponse() {
        mockWebServer.enqueue(new MockResponse().setResponseCode(404));
    }

    @Test(expected = JsonParseException.class)
    public void failsWithJsonParseErrorOnUnknownItemName() throws IOException {
        prepareUnknownItemJsonResponse();

        Call<OrderedItemsWrapper> call = sampleApi.getSampleData();
        call.execute();
    }

    private void prepareUnknownItemJsonResponse() {
        TestJsonLoader jsonLoader = new TestJsonLoader();
        String unknownNameJson = jsonLoader.loadJson("sample_data_unknown_name.json");

        mockWebServer.enqueue(new MockResponse().setResponseCode(200).setBody(unknownNameJson));
    }

    @Test(expected = JsonParseException.class)
    public void failsWithJsonParseExceptionOnMalformedJson() throws IOException {
        prepareMalformedJsonResponse();

        Call<OrderedItemsWrapper> call = sampleApi.getSampleData();
        call.execute();
    }

    private void prepareMalformedJsonResponse() {
        TestJsonLoader jsonLoader = new TestJsonLoader();
        String malformedJson = jsonLoader.loadJson("sample_data.json").substring(0, 10);

        mockWebServer.enqueue(new MockResponse().setResponseCode(200).setBody(malformedJson));
    }
}
