package my.projects.simplemoxyjsonviewer;

import android.view.View;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import androidx.recyclerview.widget.RecyclerView;

public class RecyclerViewMatcher {

    public static Matcher<View> atPositionInRecyclerView(int pos, int recyclerViewId) {
        return new TypeSafeMatcher<View>() {
            @Override
            protected boolean matchesSafely(View item) {
                RecyclerView recyclerView = item.getRootView().findViewById(recyclerViewId);
                if (recyclerView == null) {
                    return false;
                }
                RecyclerView.ViewHolder viewHolder = recyclerView.findViewHolderForAdapterPosition(pos);
                if (viewHolder == null) {
                    return false;
                }
                View viewHolderView = viewHolder.itemView;

                return item == viewHolderView;
            }

            @Override
            public void describeTo(Description description) {

            }
        };
    }
}
