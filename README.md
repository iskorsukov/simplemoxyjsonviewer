# SimpleMoxyJsonViewer

Приложение получает json с сервера и отображает полученные элементы в виде списка. Json включает элементы разных типов и порядок их отображения.

# Заметки по имплементации

## Архитектура

Приложение использует Moxy для реализации MVP архитектуры.  
(см. [MainView](/app/src/main/java/my/projects/simplemoxyjsonviewer/view/interfaces/main/MainView.java), [MainPresenter](/app/src/main/java/my/projects/simplemoxyjsonviewer/presenter/MainPresenter.java))  

Для иньекции зависимостей (кроме Presenter) используется Dagger.  
(см. [injection package](/app/src/main/java/my/projects/simplemoxyjsonviewer/injection))

## Взаимодействие с сервером

Для взаимодействия с REST API используется Retrofit.  
(см. [SampleApi](/app/src/main/java/my/projects/simplemoxyjsonviewer/model/api/SampleApi.java))  

Для десериализации используется Gson с кастомным JsonDeserializer (для обработки разных типов элементов в одном json).  
(см. [serialization package](/app/src/main/java/my/projects/simplemoxyjsonviewer/model/api/serialization/))  

Асинхронный запрос к API реализуется с использованием Call.enqueue, в Callback которого результат запроса (успех/ошибка сети/ошибка парсинга/плохой ответ сервера) обрабатывается и публикуется потребителю через RxJava's Single.  
Callback может быть заменен Observable с помощью Retrofit RxJava adapter, но из личного предпочтения и, так как запрос простой, я использовал Callback для обработки различных ситуации при осуществлении запроса.  
(см. [SampleDataService](/app/src/main/java/my/projects/simplemoxyjsonviewer/model/api/SampleDataService.java))

## Представление данных

Полученные элементы отображаются в RecyclerView. Для каждого типа элемента имеется свой ViewHolder.  
(см. [recyclerview package](/app/src/main/java/my/projects/simplemoxyjsonviewer/view/adapters/recyclerview/))  

Для отображения картинок используется Glide.  
(см. [PictureItemViewHolder](/app/src/main/java/my/projects/simplemoxyjsonviewer/view/adapters/recyclerview/viewholders/PictureItemViewHolder.java))

По клику на элемент списка отображается name элемента, который вызвал событие. Для picture отображается также text, для selector отображается также id выбранного варианта.

## Тесты
Простые интеграционные и UI тесты реализованы с использованием Mockito, JUnit и Espresso.  
(см. [androidTest package](/app/src/androidTest/java/my/projects/simplemoxyjsonviewer))

# Заметки по расширению

## Добавление новых типов  

При добавлении новых типов данных (элементов json) необходимо произвести следующее:  

* Добавить [Descriptor](/app/src/main/java/my/projects/simplemoxyjsonviewer/model/data/Descriptor.java) для нового типа данных (он используется для определения типа необходимого ViewHolder) 
* Добавить класс модели для элемента, подкласс абстрактного класса [Item](/app/src/main/java/my/projects/simplemoxyjsonviewer/model/data/Item.java) или его наследников
* Добавить десериализатор для данного типа и маппинг в [SampleItemDeserializerFactory](/app/src/main/java/my/projects/simplemoxyjsonviewer/model/api/serialization/item/SampleItemDeserializerFactory.java)
* Добавить ViewHolder для элемента, подкласс абстрактного класса [SampleItemViewHolder](/app/src/main/java/my/projects/simplemoxyjsonviewer/view/adapters/recyclerview/viewholders/SampleItemViewHolder.java) или его наследников и маппинг в [SampleItemViewHolderHelper](/app/src/main/java/my/projects/simplemoxyjsonviewer/view/adapters/recyclerview/viewholders/SampleItemViewHolderHelper.java)

При добавлении новых сообщений состояния для MainView необходимо произвести следующее:  

* (Опционально) если новое состояние связано с ответом сервера, то добавить [ResponseStatus](/app/src/main/java/my/projects/simplemoxyjsonviewer/model/api/response/ResponseStatus.java) и маппинг в [ResponseStatusToStateMapper](/app/src/main/java/my/projects/simplemoxyjsonviewer/view/state/ResponseStatusToStateMapper.java)
* Добавить [MainStateDescriptor](/app/src/main/java/my/projects/simplemoxyjsonviewer/view/state/MainStateDescriptor.java) или новую имплементацию [StateDescriptor](/app/src/main/java/my/projects/simplemoxyjsonviewer/view/state/StateDescriptor.java) 
* Добавить вариант действия для нового состояния в MainView.showStateMessage(StateDescriptor)